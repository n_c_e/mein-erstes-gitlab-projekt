# Was ist GitLab?
## Basis-Instruktion
### Registrierung und erstes Projekt

Hier steht dann was in einem Absatz, der durch Leerzeilen davor und danach markiert wird.

* Liste 1
* Liste 2

- Liste 3
- Liste 4

1. Aufzählung
1. Nächster Punkt
1. Noch ein Punkt

### Quelltext
    Das hier ist Quältext

### Link
[Git in der Wikipedia](https://de.wikipedia.org/wiki/Git)

### Bild hotgelinkt
![Jointly-Logo](https://jointly.info/)

### Ein lokales Bild
![Lokales Bild](Bilder/nils-christian-engel-foto.1024x1024.jpg)